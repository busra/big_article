---
title: "Figure3"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r message=F, warning=F}
library(tidyverse)
library(ggsignif)

source(here::here("functions.R"))

# TODO
# ~/Documents/publications/Ongoing_MS/2018-modified-tau/modified_tau/results has 18134 genes
# but  data/joined_genepair.csv has 19387
joined_genepair<-read_csv(here::here("data","joined_genepair.csv"))
```

```{r}
#determine genes based on tissue specificty consensus

consensus <- joined_genepair %>%
  select(Ensembl.Gene.ID,tissue,sample) %>%
  # just in case there are some repeats
  distinct() %>%
  dplyr::count(Ensembl.Gene.ID,tissue,name = "consensus") %>%
  arrange(desc(consensus))

consensus
```

get concensus and cancer count

```{r}

cancers <- c("brca","blca","coad","esca","kirc","kirp","luad","thca","ucec","paad","lihc")

cancer_count <- cancers %>% 
  map_df(get_cancer_count) %>% 
  distinct(gene,cancer) |> 
  add_count(gene,cancer, name = "cancer_count", sort=T) 

cancer_count_and_tissue_consensus <- consensus %>% 
  left_join(cancer_count,.,by=c("gene"="Ensembl.Gene.ID","tissue"))

# use this to find genes for Figure3
cancer_count_and_tissue_consensus %>% 
  filter(consensus > 4, cancer_count>10) %>% 
  dplyr::count(tissue)
```

let's draw images for all genes which are consensus=5 and cancer_count=11

```{r}
consensus5_cancer11 <- cancer_count_and_tissue_consensus %>% 
  filter(consensus > 4, cancer_count>10)

consensus5_cancer11 %>% 
  filter(Ensembl.Gene.ID=="ENSG00000134940")
```


```{r}

genes_of_interest <- consensus5_cancer11 %>% 
  pull(Ensembl.Gene.ID)

for (GENE in genes_of_interest){

  cancers %>% 
    map_df(~ get_selected_gene_from_cancers(.x,GENE)) %>% 
    # df from above becomes right-hand-side
    inner_join(consensus5_cancer11 %>% 
                  filter(Ensembl.Gene.ID==GENE), .,
                by=c("Ensembl.Gene.ID" = "gene","cancer")) %>% 
    ## QUESTION
    ## nn is number of cancer samples -raw
    ## then what is the n count?
    dplyr::add_count(Ensembl.Gene.ID, tissue, n ,cancer) %>% 
    write_tsv(here::here("Figures/Figure3",paste0(GENE,"_data.tsv")))

}

```





OBTAIN CALCULATED DATA FROM TCGA TO PLOT FOR SIGNIFICANT GENE (SINGNIFICANT IN 2 CANCER AND SPECIFIC TO TESTIS)
```{r,eval=FALSE}


cancers <- c("brca","blca","coad","esca","kirc","kirp","luad","thca","ucec","paad","lihc")

```

plot each gene with high consensus and high cancer count

```{r}
for (GENEPLOT in genes_of_interest){
data <- read_tsv(here::here("Figures/Figure3",paste0(GENEPLOT,"_data.tsv")))

plot_data <- data %>% 
  select(Ensembl.Gene.ID,cancer,TP,NT) %>% 
  mutate(NT=ifelse(NT==0,NA,NT)) %>% 
  gather(type,value,TP,NT) %>% 
  filter(!is.na(value)) %>% 
  mutate(value=ifelse(value>0,log2(value),0))

the_plot <- ggplot(plot_data, aes(x = type, y = value, color=type)) +
      ggdist::stat_halfeye(
        adjust = .5,
        width = .6,
        ## set slab interval to show IQR and 95% data range
        .width = c(.5, .95)
      ) +
      geom_signif(comparisons = list(c("TP","NT")),
              map_signif_level = TRUE,
              y_position=8) +
 facet_wrap(~cancer, ncol=2) +
      coord_cartesian(xlim = c(1.2, NA)) +
 ylim(0,8)
 
 ggsave(filename = here::here("Figures/Figure3",paste0(GENEPLOT,"_plot.pdf")),plot=the_plot,width = 4, height = 12)
 }
```

## Old analysis

```{r}
selected_genes_all_2cancer<-read_csv("selected_genes_all_2cancer.csv")

selected_genes_all_data<-read_csv("selected_genes_all_data.csv")
```




GENE IS SIGNIFICANTLY EXPRESSED IN 8 DIFFERENT CANCER TYPES IS SPECIFIC TO HEART
```{r}

plot1 <- selected_genes_all_data %>% 
  select(gene,cancer,TP,NT) %>% 
  mutate(TP50=ifelse(TP>50,TP,NA)) %>% 
  mutate(NT=ifelse(NT==0,NA,NT)) %>% 
  gather(type,value,TP,NT,TP50) %>% 
  filter(!is.na(value)) %>% 
  mutate(value=ifelse(value>0,log(value),0)) %>% 
  group_by(gene,cancer,type)%>% 
  mutate(count=n()) %>% 
  left_join(selected_genes_heart,by=c("gene")) %>% 
  mutate(criteria=ifelse(cancer==non_spc_cancer&type=="TP50",1,0)) %>% 
  select(-(7:9)) %>% 
  distinct() %>%  
  ungroup() %>% 
  group_by_at(vars(-criteria)) %>% 
  top_n(1,criteria) %>%
  filter(tissue=="heart") %>% 
  filter(cancer!="esca"& cancer!="kirc" & cancer!="thca") %>%
  ggplot(aes(type,value))+
  geom_boxplot(aes(fill=as.factor(criteria)))+
  ylab("Expression of \n ENSG00000129991") +
  scale_x_discrete(name = "Cancer types") +
  scale_fill_manual(values=c("0"="grey80","1"="grey20"),guide="none",
                     breaks=c("0","1"))+
  geom_signif(comparisons = list(c("TP","NT")),
              map_signif_level = TRUE,
              y_position=10)+
  # geom_signif(comparisons = list(c("TP50","NT")),
  #             map_signif_level = TRUE,
  #              y_position=12)+
 
  facet_wrap(~cancer,ncol=2,scales = "free_y")+  # ,scales = "free_y"
  coord_cartesian(ylim=c(-1,14))+
  theme_classic() +
    theme(#plot.title = element_text(size = 18, hjust = 0.5,face = "bold",
          axis.title.x = element_text(size=11,color="black",face="bold"),
          axis.title.y = element_text(size=11,color="black",face="bold"),
          strip.text.y = element_text(size=12,hjust = 0.5),
           # strip.text.x = element_blank(),
           strip.text.x = element_text(size=12,hjust = 0.5),
          panel.border=element_rect(colour = "grey",fill=NA))

plot1

```

```{r}
  ggsave("gene_8cancers.png",width=5,height = 8,dpi=300)
  ggsave("gene_8cancers.pdf",width=5,height = 8,dpi=300)
  ggsave("gene_8cancers.svg",width=5,height = 8,dpi=300)
  ggsave("gene_8cancers.jpeg",width=5,height = 8,dpi=300)
```


GENE IS SIGNIFICANTLY EXPRESSED IN 2 DIFFERENT CANCER TYPES IS SPECIFIC TO TESTIS
```{r}
selected_genes_all_2cancer<-read_csv("selected_genes_all_2cancer.csv")

selected_genes_2cancer<-data.frame(gene=c("ENSG00000124678","ENSG00000124678","ENSG00000170627","ENSG00000170627", "ENSG00000204140","ENSG00000204140" ), tissue=c("testis","testis","testis","testis","pancreas","pancreas"),cancer_caout=c(2,2,2,2,2,2),consensus=c(5,5,5,5,5,5),non_spc_cancer=c("brca","luad","brca","esca","brca","ucec"))

selected_genes_2cancer2<-data.frame(gene=c("ENSG00000170627","ENSG00000170627"), tissue=c("testis","testis"),cancer_caout=c(2,2),consensus=c(5,5),non_spc_cancer=c("brca","esca"))

library(ggdist)
plot2_data <- selected_genes_all_2cancer %>% 
  select(gene,cancer,TP,NT) %>% 
  mutate(TP50=ifelse(TP>50,TP,NA)) %>% 
  mutate(NT=ifelse(NT==0,NA,NT)) %>%
  gather(type,value,TP,NT,TP50) %>% 
  filter(!is.na(value)) %>% 
  mutate(value=ifelse(value>0,log2(value),0)) %>% 
  group_by(gene,cancer,type)%>% 
  mutate(count=n()) %>% 
  left_join(selected_genes_2cancer2,by=c("gene")) %>% 
  mutate(criteria=ifelse(cancer==non_spc_cancer&type=="TP50",1,0)) %>% 
  select(-(7:9)) %>% 
  distinct() %>%  ungroup() %>% group_by_at(vars(-criteria)) %>% 
  top_n(1,criteria)  %>%  filter(tissue=="testis") %>%  filter(cancer!="luad"& cancer!="ucec") %>% 
  ungroup() %>% 
  filter(type != "TP50") 
```

```{r}
ggplot(plot2_data, aes(x = type, y = value)) + 
  ggdist::stat_halfeye(
    adjust = .5,
    width = .6, 
    ## set slab interval to show IQR and 95% data range
    .width = c(.5, .95)
  ) + 
  ggdist::stat_dots(
    side = "left",
    dotsize = .2,
    justification = 1.05,
    binwidth = 0.2
  ) +
  coord_cartesian(xlim = c(1.2, NA))
```


```{r}
beeswarm(value ~ type, data=plot2_data)

  ggplot(aes(type,value))+
    beeswarm::beeswarm() 
  geom_boxplot(aes(fill=as.factor(criteria)))+

  
  ylab("Expression of \n ENSG00000170627" ) +
  scale_x_discrete(name = "Cancer types") +
  scale_fill_manual(values=c("0"="grey80","1"="grey20"),guide="none",
                     breaks=c("0","1"))+
  geom_signif(comparisons = list(c("TP","NT")),
              map_signif_level = TRUE,
              y_position=10)+
  # geom_signif(comparisons = list(c("TP50","NT")),
  #             map_signif_level = TRUE,
  #              y_position=12)+
 
  facet_wrap(~cancer,ncol=2,scales = "free_y")+  # ,scales = "free_y"
  coord_cartesian(ylim=c(-1,14))+
  theme_classic() +
    theme(axis.title.x = element_text(size=11,color="black",face="bold"),
          axis.title.y = element_text(size=11,color="black",face="bold"),
          strip.text.y = element_text(size=12,hjust = 0.5),
          strip.text.x = element_text(size=12,hjust = 0.5),
          panel.border=element_rect(colour = "grey",fill=NA))
plot2

```

```{r}
  ggsave("gene_2cancer.png",width=5,height = 2,dpi=300)
  ggsave("gene_2cancer.pdf",width=5,height = 2,dpi=300)
  ggsave("gene_2cancer.svg",width=5,height = 2,dpi=300)
  ggsave("gene_2cancer.jpeg",width=5,height = 2,dpi=300)
```
