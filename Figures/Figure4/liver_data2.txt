tissue	number	row	column
adipose 	13	1	1
adrenal gland	20	1	2
appendix	17	1	3
b.marrow	9	1	4
		1	5
		1	6
brain	102	1	7
breast	6	2	1
colon	19	2	2
epididymis	7	2	3
esophagus	20	2	4
heart	26	2	5
kidney	32	2	6
lung	15	2	7
lymph node	1	3	1
ovary	13	3	2
oviduct	9	3	3
pancreas	23	3	4
penis	6	3	5
placenta	27	3	6
prostate	14	3	7
rectum	8	4	1
salivary gland	12	4	2
seminal vesicle	4	4	3
skin	18	4	4
small intestine	26	4	5
spleen	12	4	6
stomach	10	4	7
		5	1
testis	73	5	2
thyroid	6	5	3
tibial nerve	4	5	4
tongue	22	5	5
uterus	2	5	6
vagina	7	5	7
v.deferens	6	6	1
w.blood	2	6	2
