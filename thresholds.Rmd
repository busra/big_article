---
title: "thresholds"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## 50 


```{r}
library(tidyverse)

raw_expression_percentage_all<-readRDS("Figures/Figure2/raw_expression_percentage_all.rds")

raw_expression_percentage_all

brca<- readRDS("Figures/Figure2/brca_only_calculated.rds")

raw<- read.table("raw_expression_all.csv", header = TRUE, sep=",")

```

